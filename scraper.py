#! /usr/bin/env python
from bs4 import BeautifulSoup
from urllib import parse
from dateutil.parser import parser
from collections import defaultdict
from requests import session
from getpass import getpass
from sys import argv, exit
from time import time



class Scraper:

    def __init__(self, session, diaspora_base_url):
        self.session = session
        self.diaspora_base_url = diaspora_base_url
        self.diaspora_stream_url = diaspora_base_url + "/stream"
        self.common_headers = {"Accept": "application/json, text/javascript, */*; q=0.01",
                               "Referer": self.diaspora_stream_url,
                               "X-Requested-With": "XMLHttpRequest"}

    def _get_login_page(self, url):
        request = self.session.get(url)
        if request.status_code != 200:
            raise Exception("Received response code " + request.status_code)
        return BeautifulSoup(request.text, 'html.parser')

    def _get_input_value(self, login_soup, field_name):
        form = login_soup.find(id="new_user")
        input_tag = form.find(attrs={"name": field_name})
        return input_tag['value']

    def _get_csrf_token(self, login_soup):
        return self._get_input_value(login_soup, "authenticity_token")

    def _get_utf8_value(self, login_soup):
        return self._get_input_value(login_soup, "utf8")

    def _get_login_target(self, login_soup):
        form = login_soup.find(id="new_user")
        return form['action']

    def _get_login_post_url(self, base_url, login_action_path):
        parse_result = parse.urlparse(base_url)
        return parse.urlunparse((parse_result.scheme or "https", parse_result.netloc, login_action_path, "", "", ""))

    def _post_login_form(self, url, username, password, csrf_token, utf8):
        return self.session.post(url, data={
            "user[username]": username,
            "user[password]": password,
            "user[remember_me]": 1,
            "authenticity_token": csrf_token,
            "utf8": utf8,
            "commit": "Sign+in"
        })

    def login(self, username, password):
        login_soup = self._get_login_page(self.diaspora_base_url)
        csrf_token = self._get_csrf_token(login_soup)
        utf8_value = self._get_utf8_value(login_soup)
        login_url = self._get_login_post_url(self.diaspora_base_url, self._get_login_target(login_soup))
        login_response = self._post_login_form(login_url, username, password, csrf_token, utf8_value)
        return login_response

    def get_posts(self, timestamp, num_posts):
        posts = []
        max_timestamp = timestamp
        date_parser = parser()
        while len(posts) < num_posts:
            posts_response = self.session.get(self.diaspora_stream_url, params={"max_time": max_timestamp},
                                         headers=self.common_headers)
            new_posts = posts_response.json()
            posts.extend(new_posts[0:num_posts - len(posts)])
            max_timestamp = date_parser.parse(new_posts[len(new_posts) - 1]["created_at"]).timestamp()
        return posts

    def _get_post_activity_level(self, post):
        return post["interactions"]["likes_count"] + post["interactions"]["comments_count"]

    def get_most_active_post(self, posts):
        most_active_post = posts[0]
        max_activity = self._get_post_activity_level(posts[0])
        for post in posts:
            activity_level = self._get_post_activity_level(post)
            if activity_level > max_activity:
                most_active_post = post
                max_activity = activity_level
        return most_active_post

    def get_post_likes(self, post_id):
        likes_url = self.diaspora_base_url + "/posts/" + str(post_id) + "/likes"
        likes_response = self.session.get(likes_url, headers=self.common_headers)
        return likes_response.json()

    def get_post_comments(self, post_id):
        comments_url = self.diaspora_base_url + "/posts/" + str(post_id) + "/comments"
        comments_response = self.session.get(comments_url, headers=self.common_headers)
        return comments_response.json()

    def get_most_active_author(self, posts):
        authors = defaultdict(lambda: 0)
        for post in posts:
            authors[post["author"]["diaspora_id"]] += 1
            likes = self.get_post_likes(post["id"])
            comments = self.get_post_comments(post["id"])
            for like in likes:
                authors[like["author"]["diaspora_id"]] += 1
            for comment in comments:
                authors[comment["author"]["diaspora_id"]] += 1
        max_active_author = ""
        max_activity_count = 0
        for activity_count in authors.items():
            if activity_count[1] > max_activity_count:
                max_active_author = activity_count[0]
                max_activity_count = activity_count[1]
        return (max_active_author, max_activity_count)

    def get_post_url(self, post_id):
        return self.diaspora_base_url + "/posts/" + str(post_id)

if __name__ == "__main__":
    if len(argv) < 3:
        print("Usage: scraper.py <diaspora username> <number of posts to analyze>")
        exit(1)
    s = session()
    scraper = Scraper(s, "https://www.joindiaspora.com")
    username = argv[1]
    password = getpass("Enter password: ")
    scraper.login(username, password)
    num_posts = int(argv[2])
    posts = scraper.get_posts(int(time()), num_posts)
    most_active_post = scraper.get_most_active_post(posts)
    most_active_user = scraper.get_most_active_author(posts)
    print("The most active post is " + scraper.get_post_url(most_active_post["id"]) + " with " +
          str(most_active_post["interactions"]["likes_count"]) + " likes and " + str(most_active_post["interactions"][
              "comments_count"]) + " comments")
    print("The most active author is " + most_active_user[0] + " with " + str(most_active_user[1]) + " likes and comments")
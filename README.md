# Diaspora Scraper:

## Setup
1. [Create a python virtualenv](https://virtualenv.pypa.io/en/stable/userguide/#usage), and activate it
2. `pip install -r requirements.txt`

## Usage
`./scraper.py <username> <number of posts to analyze>`